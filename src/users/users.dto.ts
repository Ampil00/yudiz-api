import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString, Length, Max } from 'class-validator';

export class UserCreateDTO {
	_id:string
    @ApiModelProperty()
    @IsString()
	userName: string;
 
    @ApiModelProperty()
    @IsNotEmpty()
	mobileNumber: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
	email: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
	@Length(6, 15)
	password: string

	role?: string
	salt?: string;
}

export class LoginDTO {
	@ApiModelProperty()
	@IsNumber()
	userName: string;

	@ApiModelProperty()
    @IsString()
	password: string;
}

export class LoginResponseDTO {
	@ApiModelProperty()
    @IsString()
	token: string;

	@ApiModelProperty()
    @IsString()
	role: string;

	@ApiModelProperty()
    @IsString()
	id: string;
}

export class ResponseLogin {
	@ApiModelProperty()
    @IsString()
	response_code: string;

	@ApiModelProperty()
	response_data: LoginResponseDTO;
}
export class ResponseSuccessMessage {
	@ApiModelProperty()
    @IsString()
	response_code: string;


	@ApiModelProperty()
    @IsString()
	response_data: string;
}

export class ResponseBadRequestMessage {

	@ApiModelProperty()
    @IsNumber()
	status: number;

	@ApiModelProperty()
	errors: Array<string>;

}
export class ResponseErrorMessage {

	@ApiModelProperty()
    @IsNumber()
	status: number;

	@ApiModelProperty()
    @IsString()
	message: string;
}