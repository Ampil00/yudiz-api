import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthService } from '../utils/auth.service';
import { UserCreateDTO } from './users.dto';

@Injectable()
export class UsersService {
    constructor(@InjectModel('Users') private usersModel:Model<any>,
    private authService: AuthService,){}


    public async createUser(userData: UserCreateDTO): Promise<UserCreateDTO> {
		if (userData.email) userData.email = userData.email.toLowerCase();
		const { salt, hashedPassword } = await this.authService.hashPassword(userData.password);
		userData.salt = salt;
		userData.password = hashedPassword;
		const user = await this.usersModel.create(userData);
		return user;
	}

    public async findUserByEmailOrMobile(email: string, mobileNumber: number): Promise<UserCreateDTO> {
		if (email) email = email.toLowerCase();
		const user = await this.usersModel.findOne({ $or: [{ email: email }, { mobileNumber: mobileNumber }] });
		return user;
	}

    public async getUserByMobileNo(mobileNumber: number): Promise<UserCreateDTO> {
		const user = await this.usersModel.findOne({ mobileNumber: mobileNumber });
		return user;
	}


    public async getUserById(userId: String): Promise<UserCreateDTO> {
		const user = await this.usersModel.findById(userId);
		return user;
	}

    public async getUserByEmail(email:string ): Promise<UserCreateDTO> {
		const user = await this.usersModel.findOne({ email: email });
		return user;
	}

    public async findUserByMobile(mobileNumber: string): Promise<UserCreateDTO> {
		return await this.usersModel.findOne({ mobileNumber: mobileNumber });
	}
    

    public async userStausForLogin(userId: string, userData:any): Promise<UserCreateDTO> {
		const user = await this.usersModel.findByIdAndUpdate(userId, userData);
		return user;
	}
}
