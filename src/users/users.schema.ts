import { Schema } from 'mongoose';


export const UserSchema = new Schema({
    userName: { type: String },
    email: { type: String, trim: true, lowercase: true, sparse: true },
    password: { type: String },
    salt: { type: String },
    role: { type: String },
    mobileNumber: { type: Number },
    status: { type: Boolean, default: false }

}, {
    timestamps: true
});
